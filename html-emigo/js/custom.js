function noo_shortcode_product_lider() {
    "use strict";
    $(".noo-sh-slider .noo-slider").each(function() {
        $(this).owlCarousel({
            autoPlay: false,
            items: 4,
            itemsDesktop: [ 1199, 3 ],
            itemsDesktopSmall: [ 979, 2 ],
            navigation: true,
            slideSpeed: 600,
            pagination: false
        });
    });
}

$(document).ready(function() {
    $(".noo-icon-search").on("click", function() {
        $(".search-header5").fadeIn(1).addClass("search-header-eff");
        $(".search-header5").find('input[type="search"]').val("").attr("placeholder", "").select();
        return false;
    });
    $(".remove-form").on("click", function() {
        $(".search-header5").fadeOut(1).removeClass("search-header-eff");
    });
    $("span.noo-list").on("click", function() {
        $("span.noo-grid").removeClass("active");
        $(this).addClass("active");
        $(".noo-row").removeClass("product-grid").addClass("product-list");
        setCookie("product-list", "list", 30);
        noo_removeCookie("product-grid");
    });
    $("span.noo-grid").on("click", function() {
        $("span.noo-list").removeClass("active");
        $(this).addClass("active");
        $(".noo-row").removeClass("product-list").addClass("product-grid");
        setCookie("product-grid", "grid", 30);
        noo_removeCookie("product-list");
    });
    $(".blog-owl-post").each(function() {
        $(this).owlCarousel({
            navigation: true,
            slideSpeed: 600,
            pagination: false,
            paginationSpeed: 400,
            autoHeight: true,
            addClassActive: true,
            singleItem: true
        });
    });
    if ($(".top_slider_product").length > 0) {
        var $auto = $(".top_slider_product").data("play");
        $(".top_slider_product").owlCarousel({
            autoPlay: $auto,
            items: 3,
            itemsDesktop: [ 1199, 2 ],
            itemsDesktopSmall: [ 979, 2 ],
            navigation: true,
            slideSpeed: 600,
            pagination: false
        });
    }
    if ($(".product-categories").length > 0) {
        var $cat = 0;
        $(".product-categories .cat-parent").append('<i class="fa fa-angle-down"></i>');
        $(".product-categories .cat-parent i").on("click", function() {
            var $this = $(this).closest(".cat-parent");
            if ($cat == 0) {
                $this.find(".children").slideDown();
                $cat = 1;
            } else {
                $this.find(".children").slideUp();
                $cat = 0;
            }
        });
    }
    noo_shortcode_product_lider();
	
	//Partner Slider
	$('.noo_partner').each(function(){
		$(this).owlCarousel({
			items : 6,
			itemsDesktop : [1199,6],
			itemsDesktopSmall : [979,3],
			itemsTablet: [768, 3],
			itemsMobile: [479, 1],
			slideSpeed:500,
			paginationSpeed:1000,
			rewindSpeed:1000,
			autoHeight: false,
			addClassActive: true,
			autoPlay: true,
			loop:true,
			pagination: false
		});
	});
	
	//Blog
	$("ul.sliders").owlCarousel({
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		autoPlay: true,
		loop:true,
		navigation:true,
		pagination:false
	});
	
	$('.noo-blog-slider').each(function(){
		$(this).owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items : 3,
			itemsDesktop : [1199,2],
			itemsDesktopSmall : [979,2],
			navigation : true,
			slideSpeed : 300,
			pagination: false
		});
	});
	
	$('.noo-blog-creative').each(function(){
		$(this).owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			singleItem : true,
			navigation : true,
			slideSpeed : 300,
			pagination: false
		});
	});
	
	$('.noo-blog-creative li').each(function(){
		$(this).children('.slider-item2').each(function(){
			var post_thumb = $(this).find(".noo-item-thumb");
			post_thumb.css('background-image','url("' + post_thumb.attr("data-bg") + '")');
		});
	});
	
	//Testimonial
	if(!$(".noo_testimonial2").length > 0) {
		$(".noo_testimonial").each(function(){
			$(this).owlCarousel({
				autoPlay: true,
				navigation : true,
				slideSpeed : 400,
				pagination: false,
				paginationSpeed : 400,
				addClassActive: true,
				singleItem : true
			});
		});
	}

	$(".noo-sync1").owlCarousel({
		singleItem : true,
		slideSpeed : 400,
		navigation: false,
		pagination:false,
		autoHeight: true,
		afterAction : syncPosition,
		responsiveRefreshRate : 200
	});

	$(".noo-sync2").owlCarousel({
		items : 3,
		itemsDesktop      : [1199,3],
		itemsDesktopSmall     : [979,3],
		itemsTablet       : [768,3],
		itemsMobile       : [479,1],
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
			el.find(".owl-item").eq(0).addClass("synced");
		}
	});

	function syncPosition(el){
		var current = this.currentItem;
		$(".noo-sync2")
			.find(".owl-item")
			.removeClass("synced")
			.eq(current)
			.addClass("synced")
		if($(".noo-sync2").data("owlCarousel") !== undefined){
			center(current)
		}

	}

	$(".noo-sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");

		$(".noo-sync1").trigger("owl.goTo",number);
	});

	function center(number){
		var sync2visible = $(".noo-sync2").data("owlCarousel").owl.visibleItems;

		var num = number;
		var found = false;
		for(var i in sync2visible){
			if(num === sync2visible[i]){
				var found = true;
			}
		}

		if(found===false){
			if(num>sync2visible[sync2visible.length-1]){
				$(".noo-sync2").trigger("owl.goTo", num - sync2visible.length+2)
			}else{
				if(num - 1 === -1){
					num = 0;
				}
				$(".noo-sync2").trigger("owl.goTo", num);
			}
		} else if(num === sync2visible[sync2visible.length-1]){
			$(".noo-sync2").trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
			$(".noo-sync2").trigger("owl.goTo", num-1)
		}
	}
	
	//Countdown Timer
	if($(".noo_countdown_date").length > 0) {
		austDay = new Date(2018, 8 - 1,  15);
		$('.noo_countdown_date').countdown({until: austDay});
		$('#year').text(austDay.getFullYear());
	}
	
	//Fidvids
	if($(".content-featured").length > 0) {
		$(".content-featured").fitVids();
	}
	if($(".blog-item-header").length > 0) {
		$(".blog-item-header").fitVids();
	}
	if($(".content-thumb").length > 0) {
		$(".content-thumb").fitVids();
	}
	
	//Ajax popup
	if($(".noo-qucik-view").length > 0) {
		$('.noo-qucik-view').magnificPopup({
			type: 'ajax'
		});
	}
	
	//Masonry
	if($('.noo-blog-masonry').length > 0) {
		$('.noo-blog-masonry').imagesLoaded(function(){
			$('.noo-blog-masonry').isotope({
				itemSelector : '.masonry-item',
				transitionDuration : '0.8s',
				masonry : {
					'gutter' : 0
				}
			});
	
		});
	}
	
	//Product Slider
	$("#owl-carousel").owlCarousel({
		navigation : true,
		slideSpeed : 600,
		pagination: false,
		paginationSpeed : 400,
		addClassActive: true,
		singleItem : true

	});
	$('.item-img:first-child').addClass('t-active');

	$(".images").on("click", ".item-img", function(e){
		e.preventDefault();
		$('.item-img').removeClass('t-active');
		$(this).addClass('t-active');
		var number = $('.item-img').index(this);
		$('#owl-carousel').trigger("owl.goTo",number);
	});

	$('.owl-next').on("click", function() {
		$('.item-img').removeClass('t-active');
		var $index = $( ".owl-item" ).index( $( ".active" ) );
		$('.item-img').eq($index).addClass('t-active');
	});

	$('.owl-prev').on("click", function() {
		$('.item-img').removeClass('t-active');
		var $index = $( ".owl-item" ).index( $( ".active" ) );
		$('.item-img').eq($index).addClass('t-active');
	});
	
	//Jquery Flickr Feed
	if($('.noo-instagram').length > 0) {
		$('.noo-instagram ul').jflickrfeed({
			limit: 12,
			qstrings: {
				id: '8352571@N04'
			},
			itemTemplate: '<li><a class="flickr_image" href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
		}, function(data) {
			$('.flickr_image').magnificPopup({
			  type: 'image',
			  gallery:{
				enabled:true
			  }
			});
		});
	}
	
	//Toggle Accordion
	var iconOpen = 'fa fa-minus',
		iconClose = 'fa fa-plus';

	$(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
		var $target = $(e.target)
		  $target.siblings('.accordion-heading')
		  .find('.toggle-icon').toggleClass(iconOpen + ' ' + iconClose);
		  if(e.type == 'show')
			  $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
		  if(e.type == 'hide')
			  $(this).find('.accordion-toggle').not($target).removeClass('active');
	});
	
	//Load Revolution Slider
	if($("#rev_slider_1").length > 0) {
		revSlider1();
	}
	if($("#rev_slider_2").length > 0) {
		revSlider2();
	}
	if($("#rev_slider_3").length > 0) {
		revSlider3();
	}
});

$(window).load(function() {
    "use strict";
    $(".noo-page-heading").addClass("eff");
    $(".page-title").addClass("eff");
    $(".noo-page-breadcrumb").addClass("eff");
	$('#loading').fadeOut(300);
});

function revSlider1() {
	$("#rev_slider_1").show().revolution({
		sliderType:"standard",
		sliderLayout:"fullscreen",
		dottedOverlay:"none",
		delay:9000,
		navigation: {
			keyboardNavigation:"off",
			keyboard_direction: "horizontal",
			mouseScrollNavigation:"off",
			onHoverStop:"off",
			arrows: {
				style:"hades",
				enable:true,
				hide_onmobile:false,
				hide_onleave:false,
				tmp:'<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div></div>',
				left: {
					h_align:"left",
					v_align:"center",
					h_offset:20,
					v_offset:0
				},
				right: {
					h_align:"right",
					v_align:"center",
					h_offset:20,
					v_offset:0
				}
			}
		},
		visibilityLevels:[1240,1024,778,480],
		gridwidth:1240,
		gridheight:868,
		lazyType:"none",
		shadow:0,
		spinner:"spinner0",
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,
		shuffle:"off",
		autoHeight:"off",
		fullScreenAutoWidth:"off",
		fullScreenAlignForce:"off",
		fullScreenOffsetContainer: "",
		fullScreenOffset: "",
		disableProgressBar:"on",
		hideThumbsOnMobile:"off",
		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		debugMode:false,
		fallbacks: {
			simplifyAll:"off",
			nextSlideOnWindowFocus:"off",
			disableFocusListener:false,
		}
	});
}

function revSlider2() {
	$("#rev_slider_2").show().revolution({
		sliderType:"standard",
		sliderLayout:"fullwidth",
		dottedOverlay:"none",
		delay:9000,
		navigation: {
			keyboardNavigation:"off",
			keyboard_direction: "horizontal",
			mouseScrollNavigation:"off",
			onHoverStop:"off",
			arrows: {
				style:"hades",
				enable:true,
				hide_onmobile:false,
				hide_onleave:false,
				tmp:'<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div></div>',
				left: {
					h_align:"left",
					v_align:"center",
					h_offset:20,
					v_offset:0
				},
				right: {
					h_align:"right",
					v_align:"center",
					h_offset:20,
					v_offset:0
				}
			}
		},
		visibilityLevels:[1240,1024,778,480],
		gridwidth:1240,
		gridheight:770,
		lazyType:"none",
		shadow:0,
		spinner:"spinner0",
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,
		shuffle:"off",
		autoHeight:"off",
		disableProgressBar:"on",
		hideThumbsOnMobile:"off",
		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		debugMode:false,
		fallbacks: {
			simplifyAll:"off",
			nextSlideOnWindowFocus:"off",
			disableFocusListener:false,
		}
	});
}

function revSlider3() {
	$("#rev_slider_3").show().revolution({
		sliderType:"standard",
		sliderLayout:"auto",
		dottedOverlay:"none",
		delay:9000,
		navigation: {
			keyboardNavigation:"off",
			keyboard_direction: "horizontal",
			mouseScrollNavigation:"off",
			onHoverStop:"off",
			arrows: {
				style:"hades",
				enable:true,
				hide_onmobile:false,
				hide_onleave:false,
				tmp:'<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
				left: {
					h_align:"left",
					v_align:"center",
					h_offset:20,
					v_offset:0
				},
				right: {
					h_align:"right",
					v_align:"center",
					h_offset:20,
					v_offset:0
				}
			}
		},
		visibilityLevels:[1240,1024,778,480],
		gridwidth:1240,
		gridheight:820,
		lazyType:"none",
		shadow:0,
		spinner:"spinner0",
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,
		shuffle:"off",
		autoHeight:"off",
		disableProgressBar:"on",
		hideThumbsOnMobile:"off",
		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		debugMode:false,
		fallbacks: {
			simplifyAll:"off",
			nextSlideOnWindowFocus:"off",
			disableFocusListener:false,
		}
	});
}